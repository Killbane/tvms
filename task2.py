import numpy as np
import sympy as sm
import datetime as dt

np.random.seed(dt.datetime.now().microsecond)

N = 90
q = 10
intFrom = 1
intTo = 4

x, y, funcField = sm.symbols('x y F\'')
f = 1/(2 * sm.sqrt(x))

sm.pprint(sm.Eq(funcField, sm.Integral(f, (x, intFrom, x))))

I = sm.integrate(f, (x, intFrom, x))

sm.pprint(sm.Eq(funcField, I))
sm.pprint(sm.Eq(funcField, y))

solvation = sm.solve(sm.Eq(I, y), x)

sm.pprint(sm.Eq(x, solvation[0]))

F = sm.lambdify(y, solvation[0])

X = [float] * N
randNumbers = np.random.random(N)
for i in range (0, N):
    X[i] = F(randNumbers[i])

print(f"X = {X[0:q]}")

M = sm.integrate(x/(2*sm.sqrt(x)), (x, intFrom, intTo))
print(f"M = {M:.3f}")

X_ = sum(X) / N
print(f"X_ = {X_:.3f}")

D = sm.integrate((x-M)**2/(2*sm.sqrt(x)), (x, intFrom, intTo))
print(f"D = {D:.3f}")

S2 = sum((X - X_)**2) / N
print(f"S2 = {S2:.3f}")