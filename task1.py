import numpy as np
import datetime

np.random.seed(datetime.datetime.now().microsecond)

xData = [1,2,3,-50]#input().split() #1   2   3   -50
pData = [0.3,0.3,0.3,0.1]#input().split() #0.3 0.3 0.3 0.1

N=140

sum=0
x = []
xVib = 0
M = 0
D = 0
S = 0

for i in range(len(xData)):
    M+=xData[i]*pData[i]
    D+=xData[i]*xData[i]*pData[i]
    xVib+=xData[i]/len(xData)

print("M=" + str(M))
print("D="+str(D))
print("X_="+str(xVib))

randNumbers = np.random.random(N)
for i in range(N):
    newItem = randNumbers[i]
    x.append(newItem)
    arg = pData[len(xData)-1]
    for j in range(len(xData)-1):
        if(newItem>=pData[j] or newItem<=pData[j+1]):
            arg=xData[j]
    S+=(arg-xVib)**2
S/=len(x)
print("S="+str(S))